package gamecalls;
import datamanipulation.*;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.io.IOException;	
//import stuff

public class Game extends Canvas {
	private BufferStrategy strategy;
	private boolean gameRunning = true;
	
	//set up variables to hold resoultion
	public int xRes = 800;
	public int yRes = 600;

public Game() {
		// create a frame to contain our game
		JFrame container = new JFrame("Arx");
		
		// get hold the content of the frame and set up the resolution of the game
		JPanel panel = (JPanel) container.getContentPane();
		panel.setPreferredSize(new Dimension(xRes,yRes));
		panel.setLayout(null);
		
		// setup our canvas size and put it into the content of the frame
		setBounds(0,0,xRes,yRes);
		panel.add(this);
		
		// Tell AWT not to bother repainting our canvas since we're
		// going to do that our self in accelerated mode
		setIgnoreRepaint(true);
		
		// finally make the window visible 
		container.pack();
		container.setResizable(false);
		container.setVisible(true);
		
		// add a listener to respond to the user closing the window. If they
		// do we'd like to exit the game
		container.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// request the focus so key events come to us
		requestFocus();

		// create the buffering strategy which will allow AWT
		// to manage our accelerated graphics
		createBufferStrategy(2);
		strategy = getBufferStrategy();
			}
	
	public void gameLoop() throws IOException {
		long lastLoopTime = System.currentTimeMillis();
		
		// keep looping round til the game ends
		while (gameRunning) {
			// work out how long its been since the last update, this
			// will be used to calculate how far the entities should
			// move this loop
			long delta = System.currentTimeMillis() - lastLoopTime;
			lastLoopTime = System.currentTimeMillis();
			
			// Get hold of a graphics context for the accelerated 
			// surface and blank it out
			Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
			g.setColor(Color.black);
			g.fillRect(0,0,xRes,yRes);
			
			//read the file "test2" into an array
			MurrayIO murrayIO = new MurrayIO();
			String data;
			data = murrayIO.fileData("src/files/test2.txt");
			Character[] dataChar = murrayIO.toArray(data);

			
			//draw a white 100*100 block for each 1 in the file (needs to be a 8*6 grid of text)
			g.setColor(Color.white);
			
			for (int yPos = 0; yPos < 6; yPos++) {
				for (int xPos = 0; xPos < 8; xPos++) {
					if ((dataChar[(xPos+(yPos*8))]) == '1') {
						g.fillRect((xPos)*100,(yPos)*100,(xPos*100)+100,(yPos*100)+100);
					}					
				}
			}

			// clear up the graphics
			// and flip the buffer over
			//g.dispose();
			strategy.show();
			
			//loop at around 100FPS
			try { Thread.sleep(10); } catch (Exception e) {}
		}
	}
	
	
	public static void main(String argv[]) throws IOException {
		
		//main game loop
		Game g = new Game();
		g.gameLoop();
	}
}