package gamecalls;

import datamanipulation.*;
import java.awt.Canvas; //the important lines here are 69-77.
import java.awt.Color; //they are non-functional, but can be commented out, and then you can use line
import java.awt.Dimension; //79 to test the whole premise thing.
import java.awt.Graphics2D; //also, lines 4 and 17 are extra imports which are required to run the graphics side.
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.io.IOException;
import javax.swing.ImageIcon;

public class test073 extends Canvas {
	private BufferStrategy strategy;
	private boolean gameRunning = true; // all totally stolen from the main game
										// file, just needed to get the thing
										// working.

	// set up variables to hold resoultion
	public int xRes = 1024; // the resolutions i suggested. makes for a 32x16
							// tile area.
	public int yRes = 512;
	Image wall; // initialises the images. i figure working with images is
				// better than shapes for graphics and such.
	Image floor;

	public test073() {
		// create a frame to contain our game
		JFrame container = new JFrame("Arx");

		// get hold the content of the frame and set up the resolution of the
		// game
		JPanel panel = (JPanel) container.getContentPane();
		panel.setPreferredSize(new Dimension(xRes, yRes));
		panel.setLayout(null);

		// setup our canvas size and put it into the content of the frame
		setBounds(0, 0, xRes, yRes);
		panel.add(this);

		// Tell AWT not to bother repainting our canvas since we're
		// going to do that our self in accelerated mode
		setIgnoreRepaint(true);

		// finally make the window visible
		container.pack();
		container.setResizable(false);
		container.setVisible(true);

		// create the buffering strategy which will allow AWT
		// to manage our accelerated graphics
		createBufferStrategy(2);
		strategy = getBufferStrategy();
	}

	public void gameLoop() throws IOException {
		long lastLoopTime = System.currentTimeMillis();

		// keep looping round til the game ends
		while (gameRunning) {
			Graphics2D g = (Graphics2D) strategy.getDrawGraphics();

			wall = new ImageIcon(this.getClass().getResource(
					"src/files/wall.png")).getImage();
			floor = new ImageIcon(this.getClass().getResource(
					"src/files/floor.png")).getImage();
			MurrayIO murray = new MurrayIO();

			String filedata = murray.fileData("src/files/test.txt"); // takes
																		// filedata
																		// and
																		// makes
																		// a
																		// string
			String[][] blockid = new String[32][16]; // creates a array of
														// strings
			blockid = HarriStringToArray.linetolines(filedata); // makes the
																// string from
																// filedata to
																// the array

			for (int aa = 0; aa < 32; aa++) { // cycles through the 32 rows
				for (int bb = 0; bb < 16; bb++) { // cycles through the 16
													// columns
					if (blockid[aa][bb].equals("w")) { // checks if the tile
														// data is 'floor'
						g.drawImage(wall, (aa * 32), (bb * 32), null); // if so,
																		// prints
																		// a
																		// floor
																		// graphic
																		// at
																		// that
																		// tile's
																		// spot
					} else if (blockid[aa][bb].equals("w")) { // if not, checks
																// to see if
																// it's a 'wall'
																// graphic
						g.drawImage(floor, (aa * 32), (bb * 32), null); // if
																		// so,
																		// prints
																		// a
																		// wall
																		// graphic
																		// at
																		// the
																		// tile's
																		// spot.
					}
				}
			}

			g.drawImage(floor, 0, 0, null); // uncomment this line and comment
											// 69-73 for a test run of how the
											// graphics look.
			// clear up the graphics
			// and flip the buffer over
			g.dispose();
			strategy.show();

			// loop at around 100FPS
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}
		}
	}

	public static void main(String argv[]) throws IOException {

		// main game loop
		test073 g = new test073();
		g.gameLoop();
	}

}