package Murray;
public abstract class MurrayCreature extends MurrayEntity {
	private int hpMax;
	private int hp;
	private int ac;
	private int atk;
	private int dmg;
	private int id;
	
	public MurrayCreature(int xPos, int yPos, char tile, boolean hasCollision, int hpMax, int hp, int ac ,int atk, int dmg, int id) {
		super(xPos,yPos,tile,hasCollision);
		this.hpMax = hpMax;
		this.hp = hp;
		this.ac = ac;
		this.atk = atk;
		this.dmg = dmg;
		this.id = id;
	}
	
	public void setHPMax(int i) {
		hpMax = i;
	}
	
	public int getHPMax() {
		return hpMax;
	}
	
	public void setHP(int i) {
		hp = i;
	}
	
	public int getHP() {
		return hp;
	}
	
	
	public void setAC (int i) {
		ac = i;
	}
	
	public int getAC() {
		return ac;
	}
	
	
	public void setATK (int i) {
		atk = i;
	}
	
	public int getATK() {
		return atk;
	}
	
	
	public void setDMG (int i) {
		dmg = i;
	}
	
	public int getDMG() {
		return dmg;
	}
	
	public int getID() {
		return id;
	}
	public void isDead() {
		int health = getHP();
		if (health <= 0) {
			death();
		} 
	}
	public abstract void death();
}