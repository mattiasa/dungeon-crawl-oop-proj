package Murray;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;


public class MurrayDraw {
	//import images
	Image wall 		= new ImageIcon(this.getClass().getResource("wall.png")).getImage();
	Image floor 	= new ImageIcon(this.getClass().getResource("floor.png")).getImage();
	Image player 	= new ImageIcon(this.getClass().getResource("player.png")).getImage();
	Image enemy		= new ImageIcon(this.getClass().getResource("enemy.png")).getImage();
	
	//method that when passed a character and position, will draw the related image
	public void drawImage(char c, int xPos, int yPos, Graphics2D g) {
		
		if (c == 'W') {
			g.drawImage(wall,(xPos*32),(yPos*32),null);
		} else {
					
			if (c == 'F') {
				g.drawImage(floor,(xPos*32),(yPos*32),null);
			} else {
				
				if (c == 'P') {
					g.drawImage(player,(xPos*32),(yPos*32),null);
				} else {
				
					if (c == 'E') {
						g.drawImage(enemy,(xPos*32),(yPos*32),null);
					} else {
						//System.out.println("Unused symbol!");
					}
				}
			}
		}
	}

	//method that when passed a character and position, will draw the related coloured block
	public void drawColour(char c, int xPos, int yPos, Graphics2D g) {
		
		if (c == 'b') {
			g.setColor(Color.blue);
			g.fillRect((xPos)*32,(yPos)*32,32,32);
		} else {
		
			if (c == 'u') {
				g.setColor(Color.cyan);
				g.fillRect((xPos)*32,(yPos)*32,32,32);
			} else {
					
				if (c == 's') {
					g.setColor(Color.darkGray);
					g.fillRect((xPos)*32,(yPos)*32,32,32);
				} else {
					
					if (c == 'g') {
						g.setColor(Color.gray);
						g.fillRect((xPos)*32,(yPos)*32,32,32);
					} else {
		
						if (c == 'v') {
							g.setColor(Color.green);
							g.fillRect((xPos)*32,(yPos)*32,32,32);
						} else {
		
							if (c == 'l') {
								g.setColor(Color.lightGray);
								g.fillRect((xPos)*32,(yPos)*32,32,32);
							} else {
					
								if (c == 'm') {
									g.setColor(Color.magenta);
									g.fillRect((xPos)*32,(yPos)*32,32,32);
								} else {
					
									if (c == 'o') {
										g.setColor(Color.orange);
										g.fillRect((xPos)*32,(yPos)*32,32,32);
									} else {
					
										if (c == 'p') {
											g.setColor(Color.pink);
											g.fillRect((xPos)*32,(yPos)*32,32,32);
										} else {
					
											if (c == 'r') {
												g.setColor(Color.red);
												g.fillRect((xPos)*32,(yPos)*32,32,32);
											} else {
					
												if (c == 'w') {
													g.setColor(Color.white);
													g.fillRect((xPos)*32,(yPos)*32,32,32);
												} else {
					
													if (c == 'y') {
														g.setColor(Color.yellow);
														g.fillRect((xPos)*32,(yPos)*32,32,32);
													} else {
														System.out.println("Unused symbol!");
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}