package Murray;
import java.io.DataInputStream;
import java.io.IOException;	
import java.io.EOFException;	
import java.io.FileInputStream;
//import stuff

public class MurrayIO {
	public String fileData(String filename) throws IOException {
		String data = "";
		String inputLine = "";
		DataInputStream in = new DataInputStream(new FileInputStream(filename));
		try {
		while ((data = in.readLine()) != null) {
			inputLine += (data) += "\n";
		
        }
		} catch (EOFException e) {
		}
		return inputLine;
	}

	
	public Character[][] toArray2D(String original) {
		int charSkipped = 0;
		int arrayWidth = 0;
		int newlineCount = 0;
		
		for (int i = 0; i < original.length(); i++) {
			if (original.charAt(i) != '\n') {
				arrayWidth++;
			} 
			else {
			break;
			}
			
		} 
		
		for (int i = 0; i < original.length(); i++) {
			if (original.charAt(i) == '\n') {
				newlineCount++;
			}
		}
		
		Character[] tempArray = new Character[((original.length())-newlineCount)];
		
		for (int i = 0; i < original.length(); i++) {
			if (original.charAt(i) != '\n') {
				tempArray[i - charSkipped] = original.charAt(i);
			}
			else {
				charSkipped++;
			}
		}
		
		Character[][] array = new Character[arrayWidth][newlineCount];
		
		for (int yPos = 0; yPos < newlineCount; yPos++) {
			for (int xPos = 0; xPos < arrayWidth; xPos++) {
				array[xPos][yPos] = tempArray[xPos + (arrayWidth*yPos)];
			}
		}
		
		return array;
	}

	
	public MurrayEntity[][] toTileArray(Character[][] dataChar) {
		int xSize = dataChar.length;
		int ySize = dataChar[0].length;
		MurrayEntity[][] array = new MurrayEntity[xSize][ySize];
		
		for (int xPos = 0; xPos < xSize; xPos++) {
			for (int yPos = 0; yPos < ySize; yPos++) {
				if (dataChar[xPos][yPos] == 'W'){
					array[xPos][yPos] = new MurrayEntity(xPos, yPos, dataChar[xPos][yPos], true);
				}else if (dataChar[xPos][yPos] == 'F'){
					array[xPos][yPos] = new MurrayEntity(xPos, yPos, dataChar[xPos][yPos], false);
				}
			}
		}
		
		return array;
	}
	
	
	
	public MurrayEnemy[][] toMobArray(Character[][] dataChar) {
		int xSize = dataChar.length;
		int ySize = dataChar[0].length;
		MurrayEnemy[][] array = new MurrayEnemy[xSize][ySize];
		
		for (int xPos = 0; xPos < xSize; xPos++) {
			for (int yPos = 0; yPos < ySize; yPos++) {
				if (dataChar[xPos][yPos] == 'E'){
					array[xPos][yPos] = new MurrayEnemy(xPos,yPos,dataChar[xPos][yPos],true,12,12,8,2,6,1,50);
				}else if (dataChar[xPos][yPos] == '0'){
					array[xPos][yPos] = new MurrayEnemy(xPos,yPos,dataChar[xPos][yPos],false,0,0,0,0,0,0,0);
				}
			}
		}
		
		return array;
	}
}