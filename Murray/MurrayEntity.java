package Murray;
public class MurrayEntity {
	private int xPos;
	private int yPos;
	private char tile;
	private boolean hasCollision;
	
	
	public MurrayEntity(int xPos, int yPos, char tile, boolean hasCollision) {
		this.xPos = xPos;
		this.yPos = yPos;
		this.tile = tile;
		this.hasCollision = hasCollision;
	}
	
	public void incX() {
		xPos++;
	}
	
	public void decX() {
		xPos--;
	}
	
	public void incY() {
		yPos++;
	}
	
	public void decY() {
		yPos--;
	}
	
	public int getX() {
		return xPos;
	}
	
	public int getY() {
		return yPos;
	}
	
	public void setX(int i) {
		xPos = i;
	}
	
	public void setY(int i) {
		yPos = i;
	}
	public void setCollision(boolean b) {
		hasCollision = b;
	}
	public boolean getCollision() {
		return hasCollision;
	}
	
	public char getChar() {
		return tile;
	}
	
	public void setChar(char c) {
		tile = c;
	}
	

}