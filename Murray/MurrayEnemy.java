package Murray;
public class MurrayEnemy extends MurrayCreature {
	private int worthXp;

	public MurrayEnemy(int xPos, int yPos, char tile, boolean hasCollision, int hpMax, int hp, int ac ,int atk, int dmg, int id, int worthXp) {
		super(xPos,yPos,tile,hasCollision,hpMax,hp,ac,atk,dmg,id);
		this.worthXp = worthXp;
	}

	
	public void setWorthXp(int i) {
		worthXp = i;
	}
	
	public int getWorthXp() {
		return worthXp;
	}
	public void death() {
		setCollision(false);
		setChar('0');
	}
}