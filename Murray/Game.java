package Murray;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;

import java.io.IOException;	
//import stuff

public class Game extends Canvas {
	private BufferStrategy strategy;
	private boolean gameRunning = true;
	
	//set up variables to hold resoultion
	public int xRes = 1024;
	public int yRes = 512;
	public int gameSpeed = 100;
	public int tileSize = 32;
	
	//set up variables for key input
	private boolean waitingForKeyPress = true;
	public boolean leftPressed = false;
	public boolean rightPressed = false;
	public boolean upPressed = false;
	public boolean downPressed = false;
	public boolean wPressed = false;
	public boolean aPressed = false;
	public boolean sPressed = false;
	public boolean dPressed = false;
	

public Game() {
		// create a frame to contain our game
		JFrame container = new JFrame("Arx");
		
		// get hold the content of the frame and set up the resolution of the game
		JPanel panel = (JPanel) container.getContentPane();
		panel.setPreferredSize(new Dimension(xRes,yRes));
		panel.setLayout(null);
		
		// setup our canvas size and put it into the content of the frame
		setBounds(0,0,xRes,yRes);
		panel.add(this);
		
		// Tell AWT not to bother repainting our canvas since we're
		// going to do that our self in accelerated mode
		setIgnoreRepaint(true);
		
		// finally make the window visible 
		container.pack();
		container.setResizable(false);
		container.setVisible(true);
		
		// add a listener to respond to the user closing the window. If they
		// do we'd like to exit the game
		container.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		//add listener to detect key input
		addKeyListener(new MurrayInput());
		
		// request the focus so key events come to us
		requestFocus();

		// create the buffering strategy which will allow AWT
		// to manage our accelerated graphics
		createBufferStrategy(2);
		strategy = getBufferStrategy();
		}
	
	public void gameLoop(String tileFile, String mobFile) throws IOException {
		long lastLoopTime = System.currentTimeMillis();
		
		//level file loader
		MurrayIO murrayIO = new MurrayIO();
		String rawTileData = murrayIO.fileData(tileFile);
		String rawMobData = murrayIO.fileData(mobFile);
		Character[][] tileDataChar = murrayIO.toArray2D(rawTileData);
		Character[][] mobDataChar = murrayIO.toArray2D(rawMobData);
		MurrayEntity[][] tileData =  murrayIO.toTileArray(tileDataChar);
		MurrayEnemy[][] mobData =  murrayIO.toMobArray(mobDataChar);
		
		//create drawer
		MurrayDraw murrayDraw = new MurrayDraw();
		
		//create entities
		MurrayPlayer player = new MurrayPlayer(1,1,'P',false, 20, 20, 15, 5, 8, 0, 0);
		
		//create combat
		MurrayFight combat = new MurrayFight();
		
		// keep looping round til the game ends
		while (gameRunning) {
		
			// work out how long its been since the last update, this
			// will be used to calculate how far the entities should
			// move this loop
			long delta = System.currentTimeMillis() - lastLoopTime;
			lastLoopTime = System.currentTimeMillis();
			
			
			
			//blank out window
			Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
			g.setColor(Color.black);
			g.fillRect(0,0,xRes,yRes);
			
			//draw whatever tiles have been loaded from the level file
			for (int yPos = 0; yPos < tileData[0].length; yPos++) {
				for (int xPos = 0; xPos < tileData.length; xPos++) {
					//murrayDraw.drawColour(dataChar[xPos][yPos],xPos,yPos,g);
					murrayDraw.drawImage((tileData[xPos][yPos].getChar()),xPos,yPos,g);
				}
			}
			
			//draw whtever enemies are present

			for (int yPos = 0; yPos < mobData[0].length; yPos++) {
				for (int xPos = 0; xPos < mobData.length; xPos++) {
					//murrayDraw.drawColour(dataChar[xPos][yPos],xPos,yPos,g);
					//murrayDraw.drawImage((tileData[xPos][yPos].getChar()),xPos,yPos,g);
					murrayDraw.drawImage((mobData[xPos][yPos].getChar()),xPos,yPos,g);
				}
			}
			
			
			
			//user input allows the palyer to fight nearby enemies
			if (rightPressed && !downPressed && !upPressed) {
				if (mobData[player.getX()+1][player.getY()].getCollision()) {
					combat.attack(player, mobData[player.getX()+1][player.getY()]);
				}
			}
			
			if (downPressed && !leftPressed && !rightPressed) {
				if (mobData[player.getX()][player.getY()+1].getCollision()) {
					combat.attack(player, mobData[player.getX()][player.getY()+1]);
				}
			}
			
			if (leftPressed && !downPressed && !upPressed) {
				if (mobData[player.getX()-1][player.getY()].getCollision()) {
					combat.attack(player, mobData[player.getX()-1][player.getY()]);
				}
			}
			
			if (upPressed && !leftPressed && !rightPressed) {
				if (mobData[player.getX()][player.getY()-1].getCollision()){
					combat.attack(player, mobData[player.getX()][player.getY()-1]);
				}
			}
			
			
			
			//user input moves the player if the collision of the next tile over in the appropriate direction allows it
			if (dPressed && !sPressed && !wPressed) {
				if ((!tileData[player.getX()+1][player.getY()].getCollision()) && (!mobData[player.getX()+1][player.getY()].getCollision())) {
					player.incX();
				}
			}
			
			if (sPressed && !aPressed && !dPressed) {
				if ((!tileData[player.getX()][player.getY()+1].getCollision()) && (!mobData[player.getX()][player.getY()+1].getCollision())) {
					player.incY();
				}
			}
			
			if (aPressed && !sPressed && !wPressed) {
				if ((!tileData[player.getX()-1][player.getY()].getCollision()) && (!mobData[player.getX()-1][player.getY()].getCollision())) {
					player.decX();
				}
			}
			
			if (wPressed && !aPressed && !dPressed) {
				if ((!tileData[player.getX()][player.getY()-1].getCollision()) && (!mobData[player.getX()][player.getY()-1].getCollision())){
					player.decY();
				}
			}

			
			
			//semi-redundant code; keeps player withing window boundry. Could be removed for conciseness, but may prevent bugs!
			int tilesX = xRes / tileSize;
			int tilesY = yRes / tileSize;
			
			if (player.getX() >= tilesX) {
				player.setX(tilesX-1);
			}
			
			if (player.getY() >= tilesY) {
				player.setY(tilesY-1);
			}
			
			if (player.getX() <= 0) {
				player.setX(0);
			}

			if (player.getY() <= 0) {
				player.setY(0);
			}
			
			
			
			//draw the player
			murrayDraw.drawImage(player.getChar(),player.getX(),player.getY(),g);

			
			
			//flip the buffer, showing the graphics that have been set up after some garbage disposal
			g.dispose();
			strategy.show();
			
			//loop at (*1000 divided by gameSpeed*)FPS
			try { Thread.sleep(gameSpeed); } catch (Exception e) {}
		}
	}

	
	//key input class, couldn't implement it as an external class
	public class MurrayInput extends KeyAdapter {

		//Not sure how this stuff works - don't fully understand events - but I think it essentially checks if a key has been detected as changing state. 
		//The default is that the key starts not prressed, and then it checks wether or not an event(s) has occurred.
		//If one event has occurred, this means that the key has been pressed and not released, so it is down.
		//If two, the key has been pressed and released, so it is not down.
		public void keyPressed(KeyEvent event) {	
			if (event.getKeyCode() == KeyEvent.VK_LEFT) {
				leftPressed = true;
			}
			if (event.getKeyCode() == KeyEvent.VK_RIGHT) {
				rightPressed = true;
			}
			if (event.getKeyCode() == KeyEvent.VK_UP) {
				upPressed = true;
			}
			if (event.getKeyCode() == KeyEvent.VK_DOWN) {
				downPressed = true;
			}
			if (event.getKeyCode() == KeyEvent.VK_W) {
				wPressed = true;
			}
			if (event.getKeyCode() == KeyEvent.VK_A) {
				aPressed = true;
			}
			if (event.getKeyCode() == KeyEvent.VK_S) {
				sPressed = true;
			}
			if (event.getKeyCode() == KeyEvent.VK_D) {
				dPressed = true;
			}
		}

		
		public void keyReleased(KeyEvent event) {
			if (event.getKeyCode() == KeyEvent.VK_LEFT) {
				leftPressed = false;
			}
			if (event.getKeyCode() == KeyEvent.VK_RIGHT) {
				rightPressed = false;
			}
			if (event.getKeyCode() == KeyEvent.VK_UP) {
				upPressed = false;
			}
			if (event.getKeyCode() == KeyEvent.VK_DOWN) {
				downPressed = false;
			}
			if (event.getKeyCode() == KeyEvent.VK_W) {
				wPressed = false;
			}
			if (event.getKeyCode() == KeyEvent.VK_A) {
				aPressed = false;
			}
			if (event.getKeyCode() == KeyEvent.VK_S) {
				sPressed = false;
			}
			if (event.getKeyCode() == KeyEvent.VK_D) {
				dPressed = false;
			}
		}
	}
	
	
	public static void main(String[] args) throws IOException {

		String levelFile = "src/Murray/test0.txt";
		String mobFile = "src/Murray/test1.txt";
		//String levelFile = args[0];
		//String mobFile = args[1];

		
		//main game loop
		Game g = new Game();
		g.gameLoop(levelFile, mobFile);
	}
}