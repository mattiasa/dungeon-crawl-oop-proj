package Murray;
import java.util.Random;

public class MurrayDice {
	public int roll(int sides) {
		Random generator = new Random();
		int r = generator.nextInt(sides);
		return (r+1); 
	}
}