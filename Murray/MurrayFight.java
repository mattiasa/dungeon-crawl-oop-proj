package Murray;
public class MurrayFight {
	public void attack (MurrayPlayer p, MurrayEnemy e) {
		//create a randomiser
		MurrayDice dice = new MurrayDice();
	
		//player attacks enemy
		
		
		//relevant player stats
		int pATK = p.getATK();
		int pDMG = p.getDMG();
		
		//relevant enemy stats
		int eHP = e.getHP();
		int eAC = e.getAC();
		
		//calculate damage
		int attackRoll = dice.roll(20) + pATK;
		int damageRoll = dice.roll(pDMG);
		
		if (attackRoll >= eAC) {
			System.out.println("Hit the enemy for " + damageRoll + " damage!");
			System.out.print("The enemy drops from " + eHP + "HP to ");
			eHP -= damageRoll;
			e.setHP(eHP);
			System.out.println(eHP + "HP!");
		} else {
			System.out.println("You miss the enemy!");
		}
		e.isDead();
		
		//enemy attacks player
	
	
		//relevant enemy stats
		int eATK = e.getATK();
		int eDMG = e.getDMG();
		
		//relevant player stats
		int pHP = p.getHP();
		int pAC = p.getAC();
		
		//calculate damage
		attackRoll = dice.roll(20) + eATK;
		damageRoll = dice.roll(eDMG);
		
		if (attackRoll >= pAC) {
			System.out.println("Enemy hits you for " + damageRoll + " damage!");
			System.out.print("You drop from " + pHP + "HP to ");
			pHP -= damageRoll;
			p.setHP(pHP);
			System.out.println(pHP + "HP!");
		} else {
			System.out.println("The enemy misses you!");
		}
		p.isDead();
	
		System.out.println();
	}
}