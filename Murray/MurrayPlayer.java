package Murray;
public class MurrayPlayer extends MurrayCreature {
	private int xp;

	public MurrayPlayer(int xPos, int yPos, char tile, boolean hasCollision, int hpMax, int hp, int ac,int atk, int dmg, int id, int xp) {
		super(xPos,yPos,tile,hasCollision,hpMax,hp,ac,atk,dmg,id);
		this.xp = xp;
	}
	
	public void setXP(int i) {
		xp = i;
	}
	
	public int getXP() {
		return xp;
	}
	public void death() {
		System.exit(0);
	}
}