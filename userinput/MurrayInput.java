package userinput;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MurrayInput extends KeyAdapter {
	private boolean waitingForKeyPress = true;
	private boolean leftPressed = false;
	private boolean rightPressed = false;
	private boolean upPressed = false;
	private boolean downPressed = false;

	public String returnInput() {
		String keyPressed = "none";	
		if (leftPressed) {
			keyPressed = "left";
		}
		return keyPressed;
	}
	
	
	public void keyPressed(KeyEvent event) {	
			
		if (event.getKeyCode() == KeyEvent.VK_LEFT) {
			leftPressed = true;
		}
		if (event.getKeyCode() == KeyEvent.VK_RIGHT) {
			rightPressed = true;
		}
		if (event.getKeyCode() == KeyEvent.VK_UP) {
			upPressed = true;
		}
		if (event.getKeyCode() == KeyEvent.VK_DOWN) {
			downPressed = true;
		}
	}

		public void keyReleased(KeyEvent event) {

			
		if (event.getKeyCode() == KeyEvent.VK_LEFT) {
			leftPressed = false;
		}
		if (event.getKeyCode() == KeyEvent.VK_RIGHT) {
			rightPressed = false;
		}
		if (event.getKeyCode() == KeyEvent.VK_UP) {
			upPressed = false;
		}
		if (event.getKeyCode() == KeyEvent.VK_DOWN) {
			downPressed = false;
		}
	}
}