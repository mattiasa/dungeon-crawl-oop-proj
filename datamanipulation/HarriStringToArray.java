package datamanipulation;
public class HarriStringToArray{  	//this program/function/thing takes 512-letter string, and breaks it down into a more easily handled array. 
									//for now, uses user input. if someone could tweak this to use file handling instead, that'd be awesome.

	public static void main(String args[]){
		String leveldata = args[0];					//sets leveldata string to first input. neccessary for testing only.
		String[][] blockid = new String[32][16];	//sets up a 32 by 16 2d array - each item reprisents a tile on the screen.
		blockid = linetolines(leveldata);					//calls level to use leveldata, and returns the blockid. 
		for(int height = 0; height < 16; height++){				//outputs the array in an organised format. 
			for(int width = 0; width < 32; width++){			//mostly for testing.
				System.out.print((blockid[width][height]) + " ");
			}
			System.out.println();
		}	
	}
		public static String[][] linetolines(String levelline){
		String line = levelline;						//takes in the input string.
		String[] lines = new String[16];				//defines a 1d array, 16 in length.
		String[][] linez = new String[32][16];			//defines a 2d array, 32 by 16.
		for(int i = 0; i < 16; i++){					//cycles through 16 times
			lines[i] = line.substring(0,32);			//takes the first 32 characters from the input string, stores them in part of the 1d array
			line = line.substring(32);					//removes those characters from the input string.
		}												//repeats this until the input string becomes an array of 16 32-letter strings
		for(int w = 0; w < 32; w++){					//cycles through 32 times
			for(int l = 0; l <16; l++){					//cycles a nested loop 16 times.
				linez[w][l] = lines[l].substring(w,(w+1));	//takes each 32-letter string and breaks it up into 32 1-letter strings, stores them in a line of the 2d array
			}					//does this until the whole input string is broken up into single letter items,
		} 						//and stores them in a 2d array.
		return linez;			//returns this array.
	}
}