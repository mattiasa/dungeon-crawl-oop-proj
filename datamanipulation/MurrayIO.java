package datamanipulation;
import java.io.DataInputStream;
import java.io.IOException;	
import java.io.EOFException;	
import java.io.FileInputStream;
//import stuff

public class MurrayIO {
	
	public String fileData(String filename) throws IOException {
		// I made this class static so it can be called without having to
		// create an object.
		String data = "";
		String inputLine = "";
		
		DataInputStream in = new DataInputStream(new FileInputStream(filename));
		try {
		while ((data = in.readLine()) != null) {
			inputLine += (data) += "\n";
		
        }
		} catch (EOFException e) {
		}
		return inputLine;
	}

	
	public Character[] toArray(String original) {
		int newlineCount = 0;
		int charSkipped = 0;
		for (int i = 0; i < original.length(); i++) {
			if (original.charAt(i) == '\n') {
				newlineCount++;
			}
		}
		Character[] array = new Character[(original.length())-newlineCount];
		for (int i = 0; i < original.length(); i++) {
			if (original.charAt(i) != '\n') {
				array[i - charSkipped] = original.charAt(i);
			}
			else {
				charSkipped++;
			}
		}
		return array;
	}


	public Integer[] toArrayInt(String original) {
		int newlineCount = 0;
		int charSkipped = 0;
		for (int i = 0; i < original.length(); i++) {
			if (original.charAt(i) == '\n') {
				newlineCount++;
			}
		}
		Integer[] array = new Integer[(original.length())-newlineCount];
		for (int i = 0; i < original.length(); i++) {
			if (original.charAt(i) != '\n') {
				char c = original.charAt(i);
				int a = c;
				array[i - charSkipped] = a;
			}
			else {
				charSkipped++;
			}
		}
		return array;
	}
}