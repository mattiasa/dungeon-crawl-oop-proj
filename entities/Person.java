package entities;
public class Person {
	// basic attributes will be needed
	// position is probably the most basic and key
	// Other important ones are things such as picture to determain what they look like
	// One could probably be level
	// this could be used to understand what other stats the person has
	// max health, current health, mana?
	int start_x;
	int start_y;
	//start coordinates for the character. Their own variable
	//so character can be returned to start. eg respawn.
	int x;
	int y;
	//position coordinates. Start at start. 
	int level;
	// I was thinking of adding a experience but this will only be needed for the hero
	double max_health; 
	double current_health;
	// I set these as double in case any non full integer damage can be taken
	// 
	public Person(int new_x, int new_y, int new_level, double health) {
		// object constructor. This will set all the attributes at object creation
		start_x = new_x;//called new_x and new_y so as not to clash with the attributes
		start_y = new_y;
		x = start_x;
		y = start_y;
		level = new_level;
		max_health = health;
		current_health = max_health;
		
	}
	public Person() {
		start_x = 0;
		start_y = 0;
		x = start_x;
		y = start_y;
		level = 1;
		max_health = 10;
		current_health = max_health;
	}
	public void updatePos(String input) {
		// not sure what input/output this will have.
		// it will take the input from keyboard and change the coordinates
		// for now the input will be a string of saying "UP" "DOWN" "LEFT" or "RIGHT"
		if (input == "UP") {
			y += 1;
		} else {
			if (input == "DOWN") {
				y -= 1;
			} else {
				if (input == "LEFT") {
					x -= 1;
				} else {
					if (input == "RIGHT") {
						x += 1;
					}
				}
			}
		}
		// Might be easier if this is changed to a switch case. 
		// I will leave it as is until I know what the actual input will be
		// This method should probably also call a graphics method to tell it where it has moved to
		
	}
	public void checkDead() {
		// simply checks if the health of the person has gone to or below zero. 
		// calls the dead method if it has
		if (current_health <= 0) {
			dead();
		}
	}
	public void dead() {
		//insert what ever happens when something dies. 
		//probably want differnt ones for hero and for enemy.
		//enemy would give exp to hero and hero would end the game
	}
	public void takeDamage(double dmg) {
		// a method to be called when the person object takes damage
		// takes the damage taken as input and calls the checkDead method to see if the person has died
		current_health -= dmg;
		checkDead();
		
	}
	
}
