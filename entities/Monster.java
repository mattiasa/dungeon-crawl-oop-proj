package entities;
public class Monster extends Person{
	int exp_worth;
	public Monster(int new_x, int new_y, int new_level, double health, int exp) {
		super(new_x, new_y, new_level, health);
		exp_worth = exp;
	}
	//same as in hero just calling the person attributes
	public void Dead(Hero hero) { 
		// I take the hero who killed it as an argument and then call it's method to give it more exp
		// what ever happens when destruction happens plus
		hero.updateExp(exp_worth); 
	}
}
