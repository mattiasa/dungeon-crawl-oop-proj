package entities;

public class entity{
private int x;
private int y;
private String image;
private boolean canWalk;

	public entity(int xCo, int yCo, String imageName, boolean walk){
		x = xCo;
		y = yCo;
		image = imageName;
		canWalk = walk;
	}
	public int getX(){
		int getx = x;
		return getx;
	}
	public int getY(){
		int gety = y;
		return gety;
	}
	public String getImage(){
		String imagename = image;
		return imagename;
	}
	public void setX(int a){
		x = a;
	}
	public void setY(int b){
		y = b;
	}
	public void setImage(String s){
		image = s;
	}
}
