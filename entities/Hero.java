package entities;
public class Hero extends Person{
	//lvl up + exp methods, I made them in person but realized I didn't want them there
	int exp;
	int next_lvl;
	// exp and exp needed to gain the next level
	public Hero(int new_x, int new_y, int new_level, double health, int expr, int next_level){
		super(new_x, new_y, new_level, health);
		exp = expr;
		next_lvl = next_level;
		
		// call the person constructor to give all my variables from before a value
	}
	public Hero() {
		super();
		exp = 0;
		next_lvl = 10;
	}
	
	public void updateExp(int new_exp) {
		exp += new_exp;
		if (exp >= next_lvl && level < 10) {
			exp = exp - next_lvl;
			updateLvL(level);
			updateExp(0); // check so that two levels arent gained in one go
		} else {
			if (level == 10) {
				// tell the user that max level has been reached
			}
		}
	}
	public void updateLvL(int current_lvl) {
		level += 1;
		next_lvl += 5;
		max_health += 5;
		current_health = max_health;
	}
}
