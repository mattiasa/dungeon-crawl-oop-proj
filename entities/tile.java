package entities;

public class tile extends entity{
private boolean walkable;

	public tile(int xCo, int yCo, String image, boolean walk){
		super(xCo, yCo, image, false);
		walkable = walk;
	} 
	
	public boolean canWalk(){
		boolean ofTheJedi = walkable;
		return ofTheJedi;
	}
	
	public void setWall(){
		setImage("Wall");
		walkable = false;
	}
	public void setFloor(){
		setImage("Floor");
		walkable = true;
	}
	public void setDoorDown(){
		setImage("DoorDown");
		walkable = false;
	}
	
	
	
}